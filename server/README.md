# zk-SCHNAPS demo server

## Requirements

- `node` (tested with `node 16`)
- `npm`
- `MySQL` instance

## Install

```bash
npm install
```

## Setup

Rename the file `.env.example` to `.env` and change the necessary values.

## Run

```bash
npm run dev
```