const app = require('./app');
const db = require('./db');
const { APP_PORT } = require('./config');

app.listen(APP_PORT, () => {
  console.log(`Listening on http://localhost:${APP_PORT}`);
  db.sync().then(() => {
    console.log('Database synchronized');
  });
});