const express = require('express');
const morgan = require('morgan');
const helmet = require('helmet');

require('dotenv').config();

const api = require('./api');
const jwtMiddleware = require('./middleware/jwt');

const app = express();

app.use(morgan('dev'));
app.use(helmet());
app.use(express.json());

app.get('/', (req, res) => {
  res.json({
    message: 'Working 😎'
  });
});

app.use(jwtMiddleware);
app.use('/api', api);

module.exports = app;