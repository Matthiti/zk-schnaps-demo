const { DataTypes } = require('sequelize');

const db = require('../db');

const User = db.define('User', {
  firstName: {
    type: DataTypes.STRING,
    allowNull: false
  },
  lastName: {
    type: DataTypes.STRING,
    allowNull: false
  },
  username: {
    type: DataTypes.STRING,
    allowNull: false,
    unique: true
  },
  password: {
    type: DataTypes.TEXT,
    allowNull: false
  },
  salt: {
    type: DataTypes.STRING,
    allowNull: false
  }
});

module.exports = User;