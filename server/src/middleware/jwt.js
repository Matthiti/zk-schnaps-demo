const jwt = require('jsonwebtoken');

const { JWT_SECRET } = require('../config');
const User = require('../models/user');

const whitelistedPaths = ['/api/auth/register', '/api/auth/login', '/api/auth/salt'];

const jwtMiddleware = async (req, res, next) => {
  if (whitelistedPaths.some(p => req.path.startsWith(p))) {
    return next();
  }

  const authHeader = req.header('X-Authorization');
  if (!authHeader) {
    res.status(401);
    res.json({
      message: "Missing 'X-Authorization' header"
    });
    return;
  }

  if (!authHeader.startsWith('Bearer ')) {
    res.status(401);
    res.json({
      message: "Invalid 'X-Authorization' header"
    });
    return;
  }

  const token = authHeader.slice('Bearer '.length);
  try {
    const decoded = jwt.verify(token, JWT_SECRET);
    const user = await User.findByPk(decoded.subject);
    res.locals.user = user;
    next();
  } catch (_) {
    res.status(401);
    res.json({
      message: 'Invalid token'
    });
    return;
  }
}

module.exports = jwtMiddleware;