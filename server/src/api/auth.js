const fs = require('fs');
const path = require('path');
const { createHmac } = require('crypto');
const express = require('express');
const jwt = require('jsonwebtoken');
const schnapsjs = require('schnapsjs');
const Op = require('sequelize').Op;
const { BloomFilter } = require('bloom-filters');

const User = require('../models/user');
const { JWT_SECRET, SALT_HMAC_KEY } = require('../config');

const verificationKey = JSON.parse(fs.readFileSync(path.join(__dirname, '../../resources/verification_key.json')));
const saverPk = JSON.parse(fs.readFileSync(path.join(__dirname, '../../resources/saver_pk.json')));
const saverSk = JSON.parse(fs.readFileSync(path.join(__dirname, '../../resources/saver_sk.json')));
const saverVk = JSON.parse(fs.readFileSync(path.join(__dirname, '../../resources/saver_vk.json')));

const knownCommitments = new BloomFilter(100_000, 7);

const router = express.Router();

router.post('/register', async (req, res) => {
  if (!req.body) {
    res.status(400);
    res.json({
      message: 'Missing fields'
    });
    return;
  }

  const { firstName, lastName, username, passwordProof } = req.body;
  if (!firstName || !lastName || !username || !passwordProof) {
    res.status(400);
    res.json({
      message: 'Missing fields'
    });
    return;
  }

  // Check whether the username is unique
  if (await User.findOne({ where: { username } })) {
    res.status(409);
    res.json({
      message: 'Username already taken'
    });
    return;
  }

  const { publicSignals, proof, ciphertext } = passwordProof;

  // Check the proof
  if (!await schnapsjs.register.verifyProof(verificationKey, saverPk, ciphertext, publicSignals, proof)) {
    res.status(401);
    res.json({
      message: 'Invalid password'
    });
    return;
  }

  const passwordCt = JSON.stringify({
    c_0: ciphertext.c_0,
    c: ciphertext.c
  });

  // The proof is valid, so we can store the user information
  const user = await User.create({
    firstName,
    lastName,
    username,
    password: passwordCt,
    salt: publicSignals[0]
  });

  const token = generateToken(user);
  res.json({ token });
});

router.post('/login', async (req, res) => {
  if (!req.body) {
    res.status(400);
    res.json({
      message: 'Missing fields'
    });
    return;
  }

  const { username, passwordEncryption } = req.body;
  if (!username || !passwordEncryption) {
    res.status(400);
    res.json({
      message: 'Missing fields'
    });
    return;
  }

  const user = await User.findOne({ where: { username } });
  if (!user) {
    res.status(401);
    res.json({
      message: 'Username or password incorrect'
    });
    return;
  }

  // Check the encryption
  if (!await schnapsjs.login.verifyEncryption(saverPk, passwordEncryption)) {
    res.status(401);
    res.json({
      message: 'Incorrect password encryption'
    });
    return;
  }

  if (knownCommitments.has(JSON.stringify(passwordEncryption.phi.commitment.toString()))) {
    res.status(401);
    res.json({
      message: 'High probability of a replay attack, please try again'
    });
    return;
  }

  const savedPasswordEncryption = JSON.parse(user.password);

  // Check if the passwords match
  if (!await schnapsjs.login.compareEncryptions(saverSk, saverVk, savedPasswordEncryption, passwordEncryption)) {
    res.status(401);
    res.json({
      message: 'Username or password incorrect'
    });
    return;
  }

  knownCommitments.add(JSON.stringify(passwordEncryption.phi.commitment.toString()));

  const token = generateToken(user);
  res.json({ token });
});

router.get('/salt/:username', async (req, res) => {
  const { username } = req.params;
  const user = await User.findOne({ where: { username } });
  const salt = user ? user.salt : BigInt(`0x${createHmac('sha256', SALT_HMAC_KEY).update(username).digest('hex')}`).toString();

  res.json({ salt });
});

router.put('/credentials', async (req, res) => {
  if (!req.body) {
    res.status(400);
    res.json({
      message: 'Missing fields'
    });
    return;
  }

  const { username, oldPasswordEncryption, newPasswordProof } = req.body;
  if (!username || !oldPasswordEncryption || !newPasswordProof) {
    res.status(400);
    res.json({
      message: 'Missing fields'
    });
    return;
  }

  const user = res.locals.user;
  // Check whether the username is unique
  if (await User.findOne({ where: { username, id: { [Op.ne]: user.id } } })) {
    res.status(409);
    res.json({
      message: 'Username already taken'
    });
    return;
  }

  // Check the old password encryption
  if (!await schnapsjs.login.verifyEncryption(saverPk, oldPasswordEncryption)) {
    res.status(403);
    res.json({
      message: 'Incorrect old password encryption'
    });
    return;
  }

  const savedPasswordEncryption = JSON.parse(user.password);

  // Check if the passwords match
  if (!await schnapsjs.login.compareEncryptions(saverSk, saverVk, savedPasswordEncryption, oldPasswordEncryption)) {
    res.status(403);
    res.json({
      message: 'Incorrect old password'
    });
    return;
  }

  const { publicSignals, proof, ciphertext } = newPasswordProof;

  // Then we check the proof
  if (!await schnapsjs.register.verifyProof(verificationKey, saverPk, ciphertext, publicSignals, proof)) {
    res.status(401);
    res.json({
      message: 'Invalid new password'
    });
    return;
  }

  const passwordCt = JSON.stringify({
    c_0: ciphertext.c_0,
    c: ciphertext.c
  });


  await User.update(
    { username, password: passwordCt, salt: publicSignals[0] },
    { where: { id: user.id } }
  );

  res.json({ username });
});

function generateToken(user) {
  return jwt.sign({
    subject: user.id,
    issuer: 'zk-schnaps-demo',
    iat: Date.now() / 1000,
    exp: Date.now() / 1000 + 3600
  }, JWT_SECRET);
}

module.exports = router;