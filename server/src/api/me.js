const express = require('express');

const router = express.Router();

router.get('/', async (req, res) => {
  const user = { ...res.locals.user.dataValues };
  delete user.password;
  delete user.salt;
  res.json(user);
});

module.exports = router;