# zk-SCHNAPS demo client

## Requirements

- `node` (tested with `node 16`)
- `npm`

## Install

```bash
npm install
```

## Run

```bash
npm run dev
```

The web application should now be accessible at [http://localhost:3000]().

## Build

```bash
npm run build
```