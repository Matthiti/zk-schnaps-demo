const apiUrl = '/api';
const passwordEncodingFunction = c => c.charCodeAt(0) - 31;
const passwordEncodingBase = 96;

export {
  apiUrl,
  passwordEncodingFunction,
  passwordEncodingBase
}
