import { createRouter, createWebHistory } from 'vue-router';
import { isLoggedIn } from '@/services/http';

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/login',
      name: 'login',
      // route level code-splitting
      // this generates a separate chunk (About.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import('../views/Login.vue')
    },
    {
      path: '/register',
      name: 'register',
      component: () => import('../views/Register.vue')
    },
    {
      path: '/',
      name: 'home',
      component: () => import('../views/Home.vue')
    },
    {
      path: '/account',
      name: 'account',
      component: () => import('../views/Account.vue')
    }
  ]
});

router.beforeEach((to, from, next) => {
  if (!isLoggedIn() && !['login', 'register'].includes(to.name)) {
    return next({ name: 'login' });
  }
  next();
});

export default router;
