# zk-SCHNAPS demo

This application demonstrates the zk-SCHNAPS protocol described in [\[Roe22\]](http://essay.utwente.nl/93502/) using [schnapsjs](http://gitlab.com/Matthiti/schnapsjs).

## Structure

### Client

The [client](./client/README.md) folder contains the client in the protocol, which is a web application made with VueJS.

### Server

The [server](./server/README.md) folder contains the server in the protocol, which is based on Express.

### Circom

The [circom](./circom/README.md) folder contains all Circom-related things, such as the circuit and keys.
