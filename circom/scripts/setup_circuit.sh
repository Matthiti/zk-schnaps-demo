#!/bin/sh

set -e

current_path=`pwd`
current_folder=`basename "$current_path"`

home="."
if [ "$current_folder" = "scripts" ]; then
  home=".."
fi

source ./scripts/compile_circuit.sh $1

node $home/node_modules/snarkjs/cli.js groth16 setup $home/circuit/target/circuit.r1cs $home/setup/powers_of_tau/pot_final.ptau $home/setup/circuit/circuit_0000.zkey
node $home/node_modules/snarkjs/cli.js zkey contribute $home/setup/circuit/circuit_0000.zkey $home/setup/circuit/circuit_0001.zkey --name="First contribution"
node $home/node_modules/snarkjs/cli.js zkey beacon $home/setup/circuit/circuit_0001.zkey $home/setup/circuit/circuit_final.zkey 64802f4a4b8fc2539d7e7b3467f1e1f1090fd108ee6fd11cabfe0dc77ab4b0bf 10 -n="Final Beacon phase 2"
node $home/node_modules/snarkjs/cli.js zkey export verificationkey $home/setup/circuit/circuit_final.zkey $home/setup/verification_key.json