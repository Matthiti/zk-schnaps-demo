set -e

current_path=`pwd`
current_folder=`basename "$current_path"`

home="."
if [ "$current_folder" = "scripts" ]; then
  home=".."
fi

rm -rf $home/setup/powers_of_tau/*

node $home/node_modules/snarkjs/cli.js powersoftau new bn128 16 setup/powers_of_tau/pot_0000.ptau
node $home/node_modules/snarkjs/cli.js powersoftau contribute setup/powers_of_tau/pot_0000.ptau setup/powers_of_tau/pot_0001.ptau --name="First contribution"
node $home/node_modules/snarkjs/cli.js powersoftau beacon setup/powers_of_tau/pot_0001.ptau setup/powers_of_tau/pot_beacon.ptau 0000000000000000000b7b8574bc6fd285825ec2dbcbeca149121fc05b0c828c 10 -n="Final beacon"
node $home/node_modules/snarkjs/cli.js powersoftau prepare phase2 setup/powers_of_tau/pot_beacon.ptau setup/powers_of_tau/pot_final.ptau
