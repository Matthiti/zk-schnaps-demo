#!/bin/sh

set -e

current_path=`pwd`
current_folder=`basename "$current_path"`

home="."
if [ "$current_folder" = "scripts" ]; then
  home=".."
fi

rm -rf $home/circuit/target/*
rm -rf $home/setup/circuit/*

circom $home/circuit/circuit.circom --r1cs --wasm --sym -o $home/circuit/target/