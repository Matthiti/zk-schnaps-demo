# snark-authentication-demo-circom

## Requirements

- `node` (tested with `node 16`)
- `npm`
- [circom](https://docs.circom.io)

## Install

```bash
npm install
```

## Global setup

> **Choice of power**
> 
> The choice of power is dependent on the blocklist size:
> - 1k passwords: 13?
> - 10k passwords: 15
> - 100k passwords: 16

```bash
node node_modules/snarkjs/cli.js powersoftau new bn128 16 setup/powers_of_tau/pot_0000.ptau
```

### Contribute to the Ceremony

```bash
node node_modules/snarkjs/cli.js powersoftau contribute setup/powers_of_tau/pot_0000.ptau setup/powers_of_tau/pot_0001.ptau --name="First contribution"
```

### Apply a random beacon

```bash
node node_modules/snarkjs/cli.js powersoftau beacon setup/powers_of_tau/pot_0001.ptau setup/powers_of_tau/pot_beacon.ptau 0000000000000000000b7b8574bc6fd285825ec2dbcbeca149121fc05b0c828c 10 -n="Final beacon"
```

### Prepare the second phase

```bash
node node_modules/snarkjs/cli.js powersoftau prepare phase2 setup/powers_of_tau/pot_beacon.ptau setup/powers_of_tau/pot_final.ptau
```

## Circuit-dependent setup

### Compile-only

```bash
chmod +x ./scripts/compile_circuit.sh
./compile_circuit.sh
```

### Compile and setup

```bash
chmod +x ./scripts/setup_circuit.sh
./setup_circuit.sh
```

## SAVER setup

```bash
node node_modules/snarkjs/cli.js saver keygen ./setup/circuit/circuit_final.zkey 1 ./setup/saver/saver_pk.json ./setup/saver/saver_sk.json ./setup/saver/saver_vk.json
```